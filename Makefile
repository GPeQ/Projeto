all:
	pdflatex projeto.tex && bibtex projeto && pdflatex projeto.tex && pdflatex projeto.tex

clean:
	rm -rf *.aux *.log *.swp *.out _minted* *.gz *.fls *.fdb_latexmk *.toc *.idx *.lof *.lot *.bbl *.blg *.brf *.dvi
